#define NUM_STRIPS 12
#define LEDS_PER_STRIP 300 // 240
#define CHIPSET WS2812B
#define COLOR_ORDER GRB
#define FPS 1

//#define REMOTEXY_MODE__ESP32CORE_BLUETOOTH
//#define REMOTEXY_BLUETOOTH_NAME "Womb"
//#define FASTLED_ESP32_I2S true  # I2S parallel output
//#define FASTLED_ESP32_FLASH_LOCK 1
//#define FULL_DMA_BUFFER

#include <ArduinoOTA.h>
#include <ESPmDNS.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include "secrets.h"

//#include <EEPROM.h>
//#include <RemoteXY.h>
#include "FastLED.h"
#include "I2SClocklessLedDriver.h"

static uint8_t current_brightness = 100;

//// RemoteXY config
//#pragma pack(push, 1)
// uint8_t RemoteXY_CONF[] =
//  { 255,1,0,0,0,29,0,11,13,1,
//  4,128,5,13,53,8,2,26,129,0,
//  3,6,18,6,17,66,114,105,103,104,
//  116,110,101,115,115,0 };
//
//// this structure defines all the variables and events of your control
/// interface
// struct {
//
//    // input variables
//  int8_t brightness; // =0..100 slider position
//
//    // other variable
//  uint8_t connect_flag;  // =1 if wire connected, else =0
//
//} RemoteXY;
//#pragma pack(pop)

// FastLED config
CRGB leds[NUM_STRIPS][LEDS_PER_STRIP];
I2SClocklessLedDriver driver;

// const int pins[NUM_STRIPS]={13,12,14,27,26,25,33,32,15,4,5,18};
// //{0,2,4,5,12,13,14,15,16,18,19,21,22,23,25,26};
int pins[NUM_STRIPS] = {13, 12, 14, 27, 26, 25, 33, 32, 19, 21, 22, 23};
const int num_pins = sizeof(pins) / sizeof(pins[0]);

bool is_updating = false;

void setup() {
  delay(1000); // sanity delay
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.softAP("Womb", WIFI_PASSWORD, 1, 0, 3);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  //  RemoteXY_Init ();

  //  current_brightness = EEPROM.read(0);
  //  RemoteXY.brightness = current_brightness;
  //  ESP_LOGI("setup", "reading brightness %d", current_brightness);

  //  for (int i=0;i<num_pins;i++) {
  //    pinMode(pins[i], OUTPUT);
  //  }

  pinMode(LED_BUILTIN, OUTPUT);

  driver.initled((uint8_t *)leds, pins, NUM_STRIPS, LEDS_PER_STRIP, ORDER_GRB);
  driver.setBrightness(255);

  ArduinoOTA.setHostname("Womb-ESP32");
  ArduinoOTA.setPasswordHash("4aaf6025e6edece1d66b03ec394959dd");
  ArduinoOTA
      .onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH)
          type = "sketch";
        else // U_SPIFFS
          type = "filesystem";

        // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS
        // using SPIFFS.end()
        Serial.println("Start updating " + type);
        is_updating = true;
      })
      .onEnd([]() {
        Serial.println("\nEnd");
        is_updating = false;
      })
      .onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
      })
      .onError([](ota_error_t error) {
        is_updating = false;
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR)
          Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR)
          Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR)
          Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR)
          Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR)
          Serial.println("End Failed");
      });

  ArduinoOTA.begin();
}

long time1, time2, time3;
void loop() {
  ArduinoOTA.handle();

  if (is_updating) {
    return;
  }

  time1 = ESP.getCycleCount();

  //  RemoteXY_Handler ();

  //  digitalWrite(LED_BUILTIN, RemoteXY.connect_flag);

  //  if (RemoteXY.brightness != current_brightness) {
  //    current_brightness = RemoteXY.brightness;
  //    ESP_LOGI("loop", "brightness changed to %d", current_brightness);
  //
  //    EEPROM.write(0, (uint8_t)current_brightness);
  //    //FastLED.setBrightness( current_brightness * 2.55 );
  //  }

  // Fire2012WithPalette();
  //    cylon();
  cylon_phase_shifted();
  //  for (int s=0;s<NUM_STRIPS;s++) {
  //    for (int i=0;i<LEDS_PER_STRIP;i++) {
  //      leds[s][i] = CRGB::White;
  //    }
  //  }
  //  FastLED.showColor(CRGB::Green);
  //  FastLED.show();
  // FastLED.delay(1000 / FPS);
  //  delay(1000);
  time2 = ESP.getCycleCount();
  driver.showPixels();
  time3 = ESP.getCycleCount();
  //  driver.showPixelsFirstTranpose();
  // delay(1000);
  Serial.printf(
      "Calcul pixel fps:%.2f   showPixels fps:%.2f   Total fps:%.2f \n",
      (float)240000000 / (time2 - time1), (float)240000000 / (time3 - time2),
      (float)240000000 / (time3 - time1));
}

void fadeall() {
  for (int strip = 0; strip < NUM_STRIPS; strip++) {
    for (int i = 0; i < LEDS_PER_STRIP; i++) {
      leds[strip][i].nscale8_video(254);
    }
  }
}
// void fadeall() { for (int strip=0;strip<NUM_STRIPS;strip++) {for(int i = 0; i
// < LEDS_PER_STRIP; i++) { leds[strip][i] = CRGB::Black; }} }

void cylon() {
  static int direction = 1;
  static int pos = 0;
  static uint8_t hue = 0;

  fadeall();

  for (int strip = 0; strip < NUM_STRIPS; strip++) {
    leds[strip][pos] = CHSV(hue, 255, 255);
  }

  pos += direction;

  if (pos < 0) {
    pos = 0;
    direction = 1;
  } else if (pos == LEDS_PER_STRIP) {
    pos = LEDS_PER_STRIP - 1;
    direction = -1;
  }

  hue++;
  delay(1);
}

void cylon_phase_shifted() {
  static uint8_t first_run = 1;
  static int direction[NUM_STRIPS];
  static int pos[NUM_STRIPS];
  static uint8_t hue = 0;

  if (first_run) {
    first_run = 0;
    for (int strip = 0; strip < NUM_STRIPS; strip++) {
      direction[strip] = 1;
      pos[strip] = LEDS_PER_STRIP / NUM_STRIPS * strip;
    }
  }

  fadeall();

  for (int strip = 0; strip < NUM_STRIPS; strip++) {
    leds[strip][pos[strip]] = CHSV(hue, 255, 255);

    pos[strip] += direction[strip];

    if (pos[strip] < 0) {
      pos[strip] = 0;
      direction[strip] = 1;
    } else if (pos[strip] >= LEDS_PER_STRIP) {
      pos[strip] = LEDS_PER_STRIP - 1;
      direction[strip] = -1;
    }
  }

  hue++;
  delay(1);
}

// Example FX Fire2012WithPalette from FastLED

bool gReverseDirection = false;

// COOLING: How much does the air cool as it rises?
// Less cooling = taller flames.  More cooling = shorter flames.
// Default 55, suggested range 20-100
#define COOLING 55

// SPARKING: What chance (out of 255) is there that a new spark will be lit?
// Higher chance = more roaring fire.  Lower chance = more flickery fire.
// Default 120, suggested range 50-200.
#define SPARKING 120

void Fire2012WithPalette() {
  // Array of temperature readings at each simulation cell
  static uint8_t full_heat[NUM_STRIPS][LEDS_PER_STRIP];

  for (int strip = 0; strip < NUM_STRIPS; strip++) {
    uint8_t *heat = full_heat[strip];

    // Step 1.  Cool down every cell a little
    for (int i = 0; i < LEDS_PER_STRIP; i++) {
      heat[i] =
          qsub8(heat[i], random8(0, ((COOLING * 10) / LEDS_PER_STRIP) + 2));
    }

    // Step 2.  Heat from each cell drifts 'up' and diffuses a little
    for (int k = LEDS_PER_STRIP - 1; k >= 2; k--) {
      heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2]) / 3;
    }

    // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
    if (random8() < SPARKING) {
      int y = random8(7);
      heat[y] = qadd8(heat[y], random8(160, 255));
    }

    // Step 4.  Map from heat cells to LED colors
    for (int j = 0; j < LEDS_PER_STRIP; j++) {
      // Scale the heat value from 0-255 down to 0-240
      // for best results with color palettes.
      uint8_t colorindex = scale8(heat[j], 240);
      CRGB color = ColorFromPalette(HeatColors_p, colorindex);
      int pixelnumber;
      if (gReverseDirection) {
        pixelnumber = (LEDS_PER_STRIP - 1) - j;
      } else {
        pixelnumber = j;
      }
      leds[strip][pixelnumber] = color;
    }
  }
}
